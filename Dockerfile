FROM ubuntu:latest

USER root
COPY --chown=root:root packages/*.deb /tmp/

RUN dpkg --add-architecture i386 && \
    apt-get update -y && \
    apt-get install -y --no-install-recommends wget curl netcat sudo gnupg nano gettext-base libc6:i386 zlib1g:i386 libudev1:i386 libxext6:i386 && \
    apt-get autoremove && \
    apt-get clean && \
    dpkg --force-depends -i /tmp/*.deb 2>/dev/null && \
    rm -rf /tmp/*.deb /usr/share/doc /usr/share/locale /usr/share/man /usr/share/X11 /var/lib/dpkg /var/lib/apt /opt/wine-staging/share/wine/fonts

# Create a user to run as
RUN export uid=1000 gid=1000 && \
    mkdir -p /home/gamer && \
    echo "gamer:x:${uid}:${gid}:gamer,,,:/home/gamer:/bin/bash" >> /etc/passwd && \
    echo "gamer:x:${uid}:" >> /etc/group && \
    echo "gamer ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/gamer && \
    chmod 0440 /etc/sudoers.d/gamer && \
    chown ${uid}:${gid} -R /home/gamer &&\
    usermod -a -G video gamer

RUN usermod -u 1000 gamer && groupmod -g 1000 gamer

#Use created user
USER gamer
ENV HOME /home/gamer
ENV USER gamer

RUN mkdir -p "/home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/Program Files"
RUN mkdir -p "/home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/users"
COPY --chown=gamer:gamer direct /home/gamer/direct
COPY --chown=gamer:gamer test /home/gamer/test
COPY --chown=gamer:gamer del /home/gamer/del
COPY --chown=gamer:gamer ["packages/PlayOnLinux/wineprefix/SAPI5/drive_c/Python27", "/home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/Python27"]
COPY --chown=gamer:gamer ["packages/PlayOnLinux/wineprefix/SAPI5/drive_c/Program Files/Common Files", "/home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/Program Files/Common Files"]
COPY --chown=gamer:gamer packages/PlayOnLinux/wineprefix/SAPI5/drive_c/windows /home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/windows
COPY --chown=gamer:gamer ["packages/PlayOnLinux/wineprefix/SAPI5/drive_c/Program Files/NextUp-Ivona", "/home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/Program Files/NextUp-Ivona"]
COPY --chown=gamer:gamer packages/PlayOnLinux/wineprefix/SAPI5/*.* /home/gamer/.PlayOnLinux/wineprefix/SAPI5/
COPY --chown=gamer:gamer packages/PlayOnLinux/wineprefix/SAPI5/drive_c/*.* /home/gamer/.PlayOnLinux/wineprefix/SAPI5/drive_c/

USER gamer

ENV WINEPREFIX=/home/gamer/.PlayOnLinux/wineprefix/SAPI5
RUN cd $WINEPREFIX/drive_c && wine python talk.py "Hello World" "C:\build_confirmed.wav" 2>/dev/null
